.. index:: Geometry Nodes; 3D Cursor
.. _bpy.types.GeometryNodeTool3DCursor:

**************
3D Cursor Node
**************

.. figure:: /images/node-types_GeometryNodeTool3DCursor.webp
   :align: right
   :alt: 3D Cursor node.

The *3D Cursor* node outputs the position and orientation of the 3D cursor in the scene.

.. note::

   This node can only be used in the :ref:`Tool context <tool_context>`.


Inputs
======

This node has no inputs.


Properties
==========

This node has no properties.


Outputs
=======

Location
   The position of the 3D cursor.

Rotation
   The orientation of the 3D cursor as a standard rotation value.
