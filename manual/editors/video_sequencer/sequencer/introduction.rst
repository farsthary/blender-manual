
************
Introduction
************

The Sequencer view is where most of the video editing happens.
It shows a stack of :doc:`channels </editors/video_sequencer/sequencer/channels>`,
in which you can create :doc:`strips </video_editing/edit/montage/strips/introduction>`.
