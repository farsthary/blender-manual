
*******
Editing
*******

Control
=======

Snap
----

Activates automatic snapping when you moving keys.

Snap To
   Type of element to snap to.

   :Frame: Snap to frame.
   :Second: Snap to seconds.
   :Nearest Marker: Snap to nearest :doc:`Marker </animation/markers>`.

Absolute Time Snap
   Absolute time alignment when transforming keyframes


Proportional Editing
--------------------

See :doc:`Proportional Editing </editors/3dview/controls/proportional_editing>`.
